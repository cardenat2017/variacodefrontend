import React, {useState} from 'react';
import Error from './Error';

import axios from 'axios';
import Swal from 'sweetalert2';
import { withRouter } from 'react-router-dom';

function AgregarTodo({history, guardarRecargarTodos}) {

    // state
    const [ tarea, guardarTarea ] = useState('');
    const [ responsable, guardarResponsable] = useState('');
    const [ fecha, guardarFecha ] = useState('');
    const [ completada, guardarCompletada ] = useState('');
    const [ error, guardarError ] = useState(false);

    const leerValorRadio = e => {
        guardarCompletada(e.target.value);
    }

    const agregarTodo = async e => {
        e.preventDefault();

        if(tarea ==='' || responsable === '' || fecha === '' || completada === '') {
            guardarError(true);
            return;
        }

        guardarError(false);

        // Crear el nuevo To-Do
        try {
            const resultado = await axios.post('http://localhost:4000/todo', {
                tarea, 
                responsable,
                fecha,
                completada
            });

            if(resultado.status === 201) {
                Swal.fire(
                    'To-Do Creado',
                    'To-Do se creó correctamente',
                    'success'
                )
            }
        } catch (error) {
            console.log(error);
            Swal.fire({
                type: 'error',
                title: 'Error',
                text: 'Hubo un error, vuelve a intentarlo'
            })
        }

        // Redirigir al usuario a productos
        guardarRecargarTodos(true);
        history.push('/todo');

    }

    return (
        <div className="col-md-8 mx-auto ">
            <h1 className="text-center">Agregar Nuevo To-Do</h1>

            {(error) ? <Error mensaje='Todos los campos son obligatorios' /> : null }

            <form
                className="mt-5"
                onSubmit={agregarTodo}
            >
                <div className="form-group">
                    <label>Descripción To-Do</label>
                    <input 
                        type="text" 
                        className="form-control" 
                        name="tarea" 
                        placeholder="Descripción de To-Do"
                        onChange={e => guardarTarea(e.target.value)}
                    />
                </div>

                <div className="form-group">
                    <label>Responsable</label>
                    <input 
                        type="number" 
                        className="form-control" 
                        name="responsable"
                        placeholder="Responsable"
                        onChange={e => guardarResponsable(e.target.value)}
                    />
                </div>

                <div className="form-group">
                    <label>Fecha</label>
                    <input 
                        type="date" 
                        className="form-control" 
                        name="fecha"
                        placeholder="Responsable"
                        onChange={e => guardarFecha(e.target.value)}
                    />
                </div>

                <div className="form-check form-check-inline">
                    <input 
                        className="form-check-input" 
                        type="radio" 
                        name="categoria"
                        value="postre"
                        onChange={leerValorRadio}
                    />
                    <label className="form-check-label">
                        Completada
                    </label>
                </div>

                <input type="submit" className="font-weight-bold text-uppercase mt-5 btn btn-primary btn-block py-3" value="Agregar To-Do" />
            </form>
        </div>
    )
}
export default withRouter(AgregarTodo);