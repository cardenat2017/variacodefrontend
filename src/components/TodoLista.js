import React from 'react';
import Moment from 'react-moment';
import { Link } from 'react-router-dom';

import axios from 'axios';
import Swal from 'sweetalert2';

function TodoLista({todo, guardarRecargarTodos}) {


    const eliminarTodo = id => {
        console.log('eliminando', id);

        // TODO: Eliminar los registros
        Swal.fire({
            title: '¿Estas Seguro?',
            text: "Un Todo eliminado no se puede recuperar",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Eliminar',
            cancelButtonText : 'Cancelar'
        }).then( async (result) => {
            if (result.value) {
                try {
                    const url = `http://localhost:4000/todo/${id}`;
                    const resultado = await axios.delete(url);
                    if(resultado.status === 200){
                        Swal.fire(
                            'Eliminado!',
                            'El To-Do se ha eliminado',
                            'success'
                        )
                        // Consultar la api nuevamente
                        guardarRecargarTodos(true)
                    }
                } catch (error) {
                    console.log(error);
                    Swal.fire({
                        type: 'error',
                        title: 'Error',
                        text: 'Hubo un error, vuelve a intentarlo'
                    })
                }
            }
        })
    }



    return(
            <tbody>
                <tr>
                    <td>{todo.tarea}</td>
                    <td>{todo.responsable}</td>
                    <td><Moment format="DD/MM/YYYY">{todo.fecha}</Moment></td>
                    <td className="text-center"><span className="badge badge-success">Si</span>
                    </td>
                    <td>
                        <Link
                            to={`/todo/editar/${todo.id}`}
                            className="btn btn-success btn-md mr-2"> <i className="fa fa-pencil" aria-hidden="true" alt="Editar To-Do"></i>  
                        </Link>
                        <button
                            type="button"
                            className="btn btn-danger btn-md"
                            onClick={() => eliminarTodo(todo.id)}><i className="fa fa-trash" aria-hidden="true" alt="Eliminar To-Do"></i> 
                        </button>
                    </td>
                </tr>
            </tbody>
    )
}

export default TodoLista;