import React, { Component } from 'react';
import axios from 'axios';
import Swal from 'sweetalert2';

class Login extends Component{
    state = {
        correo: '',
        clave: '',
        token: ''
    }

    // Iniciar Sesión
    iniciarSesion = e => {
        e.preventDefault();
        // Enviar el Request
        const url = 'http://localhost:4000/login';
        try {
            axios.post(url, this.state)
             .then((resultado)=>{
                
                if(resultado.data.token!==''){
                    this.setState({ 
                        token: resultado.data.token 
                    });
                    this.props.history.push("/todos");
                }
                else
                    Swal.fire(
                    'Validación Usuario',
                    'Usuario / Clave invalida',
                    'error'
                )
             });
        } catch (error) {
            console.log(error);
            Swal.fire({
                type: 'error',
                title: 'Error',
                text: 'Hubo un error, vuelve a intentarlo'
            })
        }
    }

    

    leerDatos = e => {
        this.setState({
            [e.target.name] : e.target.value
        })
    }

    render(){
        return(
            <div className="row justify-content-center">
                <div className="col-md-5">
                    <div className="card mt-5">
                        <div className="card-body">
                            <h2 className="text-center py-4">
                                <i className="fa fa-lock">{' '}
                                    Iniciar Sesión
                                </i>
                            </h2>
                            <form onSubmit={this.iniciarSesion}>
                                <div className="form-group">
                                    <label>Correo: </label>
                                    <input type="email" 
                                           className="form-control"
                                           name="correo"
                                           required
                                           value={this.state.correo}
                                           onChange={this.leerDatos} 
                                    />
                                </div>
                                <div className="form-group">
                                    <label>Contraseña: </label>
                                    <input type="password" 
                                           className="form-control"
                                           name="clave"
                                           required
                                           value={this.state.clave}
                                           onChange={this.leerDatos} 
                                    />
                                </div>
                                <input type="submit" className="btn btn-success btn-block" value="Iniciar Sesión"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default Login;