import React, { Fragment} from 'react';
import TodoLista from './TodoLista';

function Todos({todos, guardarRecargarTodos}) {
    return (
        <Fragment>
            <h1 className="text-center">To-Dos</h1>
            <table className="table table-responsive mt-5" >
                <thead>
                    <tr>
                        <th width="30%">Tarea</th>
                        <th width="30%">Responsable</th>
                        <th width="10%">Fecha</th>
                        <th witdh="10%">Completada</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                {todos.map(todo => (
                    <TodoLista 
                        key={todo.id}
                        todo={todo}
                        guardarRecargarTodos={guardarRecargarTodos}
                    />
                ))}
            </table>
            
        </Fragment>
    )
}
export default Todos;