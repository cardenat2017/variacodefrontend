import React, { useState, useRef } from 'react';
import Error from './Error';

import axios from 'axios';
import Swal from 'sweetalert2';
import { withRouter } from 'react-router-dom';

function EditarTodo(props) {

    // Destructurando los props
    const {history, todo, guardarRecargarTodos} = props;

    // generar los refs
    const tareaRef = useRef('');
    const responsableRef = useRef('');
    const fechaRef = useRef('');
    const completadaRef = useRef('');

    const [ error, guardarError ] = useState(false);
    const [ completada, guardarCompletada ] = useState('');

    const editarTodo = async e => {
        e.preventDefault();

        // validacion
        const nuevaTarea = tareaRef.current.value,
              nuevoResponsable = responsableRef.current.value,
              nuevaFecha = fechaRef.current.value,
              nuevaCompletada = completadaRef.current.value;

        if(nuevaTarea === '' || nuevaTarea === '' ) {
            guardarError(true);
            return;
        }

        if(nuevoResponsable === '' || nuevoResponsable === '' ) {
            guardarError(true);
            return;
        }

        if(nuevaFecha === '' || nuevaFecha === '' ) {
            guardarError(true);
            return;
        }

        guardarError(false);
        // Revisar si cambio la tarea de lo contrario asignar el mismo valor
        let tareaCompletada = (completada === '') ? todo.completada : completada;

        // Obtener los valores del formulario
        const editarTodo = {
            tarea : nuevaTarea,
            responsable : nuevoResponsable,
            fecha: nuevaFecha,
            completada: nuevaCompletada 
        }

        // Enviar el Request
        const url = `http://localhost:4000/todo/${todo.id}`;

        try {
            const resultado = await axios.put(url, editarTodo);

            if(resultado.status === 200) {
                Swal.fire(
                    'To-Do Editado',
                    'El To-Do se editó correctamente',
                    'success'
                )
            }
        } catch (error) {
            console.log(error);
            Swal.fire({
                type: 'error',
                title: 'Error',
                text: 'Hubo un error, vuelve a intentarlo'
            })
        }

        // redirigir al usuario, consultar api
        guardarRecargarTodos(true);
        history.push('/todo');

    }

    const leerValorRadio = e => {
        guardarCompletada(e.target.value);
    }

    return (
        <div className="col-md-8 mx-auto ">
            <h1 className="text-center">Editar To-Do</h1>

            {(error) ? <Error mensaje='Todos los campos son obligatorios' /> : null }

            <form
                className="mt-5"
                onSubmit={editarTodo}
            >
                <div className="form-group">
                    <label>Tarea</label>
                    <input 
                        type="text" 
                        className="form-control" 
                        name="tarea" 
                        placeholder="Descripción de Tarea"
                        ref={tareaRef}
                        defaultValue={todo.tarea}
                    />
                </div>

                <div className="form-group">
                    <label>Responsable</label>
                    <input 
                        type="text" 
                        className="form-control" 
                        name="responsable"
                        placeholder="Responsable de Tarea"
                        ref={responsableRef}
                        defaultValue={todo.responsable}
                    />
                </div>

                <div className="form-group">
                    <label>Fecha</label>
                    <input 
                        type="date" 
                        className="form-control" 
                        name="fecha"
                    />
                </div>

                <div className="form-check form-check-inline">
                    <input 
                        className="form-check-input" 
                        type="radio" 
                        name="completada"
                        value="true"
                        onChange={leerValorRadio}
                        defaultChecked={(todo.completada === false)}
                    />
                    <label className="form-check-label">
                        Completada
                    </label>
                </div>

                <input type="submit" className="font-weight-bold text-uppercase mt-5 btn btn-primary btn-block py-3" value="Editar To-Do" />
            </form>
        </div>
    )
}
export default  withRouter(EditarTodo);