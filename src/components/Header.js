import React from 'react';
import {  Link, NavLink } from 'react-router-dom';

const Header = () => ( 
    <nav className = "navbar navbar-expand-lg navbar-dark bg-dark" >
        <div className = "container" >
            <Link to = "/todos"
                className = "navbar-brand" >
                VariaCode Test 
            </Link>
            <ul className = "navbar-nav mr-auto" >
                <li className = "nav-item" >
                    <NavLink to = '/todos'
                             className = "nav-link"
                             activeClassName = "active" >
                        To - Dos 
                    </NavLink> 
                </li>
                <li className = "nav-item" >
                    <NavLink to = '/nuevo-todo'
                             className = "nav-link"
                             activeClassName = "active">
                        Nuevo To - Do 
                    </NavLink> 
                </li> 
            </ul> 
        </div > 
    </nav>
);

export default Header;