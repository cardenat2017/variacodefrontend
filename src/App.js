import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import axios from 'axios';

import Header from './components/Header';
import Todos from './components/Todos';
import EditarTodo from './components/EditarTodo';
import AgregarTodo from './components/AgregarTodo';
import Todo from './components/Todo';
import Login from './components/Login';

function App() {
    const [todos, guardarTodos] = useState([]);
    const [recargarTodos, guardarRecargarTodos] = useState(true);

    useEffect(() => {
        if (recargarTodos) {
            const consultarApi = async() => {
                // consultar la api de json-server
                const resultado = await axios.get('http://localhost:4000/todo');

                guardarTodos(resultado.data);
            }
            consultarApi();

            // Cambiar a false la recarga de los Todos
            guardarRecargarTodos(false);
        }
    }, [recargarTodos]);

    return ( 
        <Router>
            <Header/>
            <main className = "container mt-5" >
                <Switch>
                    <Route exact path="/" render={() => (
                        // loggedIn ? (
                        //     <Redirect to="/todos"/>
                        // ) : (
                            <Redirect to="/login"/>
                        // )
                         )}
                    />
                    <Route exact path = "/Login" component={Login} /> 
                    <Route exact path = "/todos"
                        render = {
                            () => ( <
                                Todos todos = { todos }
                                guardarRecargarTodos = { guardarRecargarTodos }
                                />
                            )
                        }
                    /> 
                    <Route exact path = "/nuevo-todo"
                        render = {
                        () => ( <
                            AgregarTodo guardarRecargarTodos = { guardarRecargarTodos }
                            />
                        )
                    }
                    /> 
                    <Route exact path = "/todo/:id"
                    component = { Todo }
                    /> 
                    <Route exact path = "/todo/editar/:id"
                        render = {
                            props => {
                            // tomar el ID del To-Do
                            const id = parseInt(props.match.params.id);

                            // El To-Do que se pasa al state
                            const todo = todos.filter(todo => todo.id === id);

                            return ( <
                                EditarTodo todo = { todo[0] }
                                guardarRecargarTodos = { guardarRecargarTodos }
                                />
                            )
                        }
                    }
                    />  
                </Switch>  
            </main>  
            <p className = "mt-4 p2 text-center" > Todos los derechos Reservados </p>  
        </Router>
    );
}

export default App;