# Proyecto Frontend de Prueba para Variacode 

## Desarrollado con React 

## Requisitos de instalación: 
   NPM version 6.4.1

## Observaciones:
   La aplicación se autentica usando JWT / OAuth

## Pasos para activar la funcionalidad del Aplicativo

  1. Clonar el repositorio localmente con el siguiente comando:
	 `git clone https://cardenat2017@bitbucket.org/cardenat2017/variacodefrontend.git`
  2. Dentro del directorio variacodefrontend ejecutar:
  	 `npm install`
  3. Para que la aplicación se ejecute correctamente es necesario tener configurado el Backend, debe ir al repositorio variacodebackend y clonarlo localmente, tambien debe seguir
     instrucciones de instalación
  4. Luego que los dos repositorios están bien configurados, correr el siguiente comando de NPM:
  	 `npm start`
  5. Luego abrir navegador usando http://localhost:3000.
  6. Hacer pruebas usando las siguientes credenciales: 
     Usuario: cardenat@gmail.com
     Contrasena: 11500145

## Bugs conocidos
  Ninguno
